import account from './account'
import setting from './setting'
import artwork from './artwork'
import admin from './admin'
import users from './users'
import auction from './auction'

export default {account, setting, artwork, admin, users, auction}