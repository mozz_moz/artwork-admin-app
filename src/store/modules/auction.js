import {queryAuction} from '@/api'

const state = {
    auctions:{},
    pageIndex:0,
    pageSize:0
}

const mutations = {
    AUCTIONS(context, data){
        context.auctions = data
    },
    PAGEINFO(context, data){
        context.pageIndex = data.pageIndex
        context.pageSize = data.pageSize
    }
}

const actions = {
    async auctions({commit}, params){
        if(!params) return
        let {pageIndex, pageSize}  = params
        if(!pageIndex|| !pageSize) return
        let result = await queryAuction(pageIndex.toString(), pageSize.toString())
        commit("AUCTIONS",result)
        commit("PAGEINFO",{pageIndex, pageSize})
    }
}

const getters = {}

export default {
    state,
    mutations,
    actions,
    getters
}