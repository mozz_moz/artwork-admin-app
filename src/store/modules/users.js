import { queryusers } from "@/api/admin"
const state = {
    users:{}
}

const mutations = {
    USERS(context, data){
        context.users = data
    }
}

const actions = {
    async users({commit}, params){
        if(!params) return
        let {pageIndex, pageSize} = params
        if(!pageIndex || !pageSize) return
        let result = await queryusers(pageIndex, pageSize)
        commit("USERS",result)
    }
}

const getters = {}

export default {
    state,
    mutations,
    actions,
    getters
}