import { getShopBynotAudit,getShopDetail } from "@/api"
const state = {
    artDetail:{},
    // 未上架的艺术品列表
    artworksNotAudit:{},
    pageIndex:"0",
    pageSize:"0"
}

const mutations = {
    ARTDETAIL(context,data){
        context.artDetail = data
    },
    RESETARTDETAIL(context){
        context.artDetail = {}
    },
    ARTWORKSNOTAUDIT(context, data){
        context.artworksNotAudit = data
    },
    PAGEINFO(context, data){
        let {pageIndex, pageSize} = data
        context.pageIndex = pageIndex
        context.pageSize = pageSize
    }
}

const actions = {
    async artDetail({commit},id){
        commit("RESETARTDETAIL")
        // let result = await getArtworkByIdFake(id)
        let result = await getShopDetail(id)
        commit("ARTDETAIL", result[0])
    },
    async artworksNotAudit({commit}, params){
        let result = await getShopBynotAudit(params)
        commit('ARTWORKSNOTAUDIT',result)
        commit('PAGEINFO',params)
    }
}

const getters = {}

export default {
    state,
    mutations,
    actions,
    getters
}