const state = {
    adminInfo:{}
}

const mutations = {
    ADMININFO(context, data){
        context.adminInfo = data
    }
}

const actions = {
    adminInfo({commit}, params){
        commit("ADMININFO",params)
    }
}

const getters = {}

export default {
    state,
    mutations,
    actions,
    getters
}