
import Vue from 'vue'
import {
  Button,
  Input,
  InputNumber,
  Upload,
  Loading,
  MessageBox,
  Message,
  Notification,
  Container,
  Header,
  Aside,
  Main,
  Footer,
  Select,
  Option,
  Radio,
  RadioGroup,
  RadioButton,
  Image,
  Carousel,
  CarouselItem,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  DatePicker,
} from 'element-ui'

Vue.use(Button)
Vue.use(Input)
Vue.use(InputNumber)
Vue.use(Upload)
Vue.use(Container)
Vue.use(Header)
Vue.use(Aside)
Vue.use(Main)
Vue.use(Footer)
Vue.use(Select)
Vue.use(Option)
Vue.use(Radio);
Vue.use(RadioGroup);
Vue.use(RadioButton);
Vue.use(Image)
Vue.use(Carousel)
Vue.use(CarouselItem)
Vue.use(Dropdown);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(DatePicker)

Vue.prototype.$loading = Loading.service;
Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;
Vue.prototype.$confirm = MessageBox.confirm;
Vue.prototype.$prompt = MessageBox.prompt;
Vue.prototype.$notify = Notification;
Vue.prototype.$message = Message;