// axios 二次封装
import axios from 'axios'
import nprogress from 'nprogress'
import 'nprogress/nprogress.css'
const requsets = axios.create({
    // baseURL:'http://localhost:3001',
    // baseURL:'/',
    baseURL:'http://106.55.165.19:3000',
    timeout:5000
})

// 请求拦截器
requsets.interceptors.request.use((config)=>{
    nprogress.start()
    return config
})

// 响应拦截器
requsets.interceptors.response.use((res)=>{
    nprogress.done()
    return res.data
},(error)=>{
    nprogress.done()
    return Promise.reject(error)
})

export default requsets