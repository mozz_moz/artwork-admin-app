import request from './request'

let BaseUrl = 'api2'

// 获取所有艺术品
export const getAllArtwork = ()=>{
    return  request({url:BaseUrl+'/getAllArtwork', method:'get'})
}

// 获取所有上架的艺术品 假数据
export const getAllArtworkFake = (data)=>{
    return  request({url:BaseUrl+'/getAllArtworkFake', method:'get', params:data})
}

// 获取用户未上架的艺术品 假数据
export const getAllSoldArtworkFake = ()=>{
    return request({url:BaseUrl+'/getAllSoldArtworkFake', method:'get'})
}

// 根据id获取艺术品数据 假数据
export const getArtworkByIdFake = (id)=>{
    return request({url:BaseUrl+'/getArtworkByIdFake', method:'get', params:{id}})
}

// 根据地址获取用户数据 假数据
export const getUserByAddress = (address)=>{
    return request({url:BaseUrl+'/getUserByAddressFake', method:'get', params:{address}})
}

// 获取Banner 假数据
export const getBannersFake = ()=>{
    return request({url:BaseUrl+'/getBannersFake'})
}

// 搜索 假数据
export const getSeachArtResult = (keyword)=>{
    return request({url:BaseUrl+'/search/art', params:{keyword}})
}



export * from './admin'

// // 测试接口
// async function test(){
//     let result = await getSeachArtResult('a')
//     console.log(result)
// }
// test()