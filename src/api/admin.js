import request from './request'

const BaseUrl = '/admin/'

// 管理员登录
export const adLogin = (username, password)=>{
    return request({url:BaseUrl+'adLogin', method:'post', data:{username, password}})
} 

// 获取所有未上架的商品
export const getShopBynotAudit = (data)=>{
    return request({url:BaseUrl+'getShopBynotAudit', method:'post', data})
}


// 获取艺术品详情
export const getShopDetail = (id)=>{
    return request({url:BaseUrl + 'getShopDetail', method:'post', data:{id}})
}

// 同意上架
export const setSuccess = (id) => {
    return request({url:BaseUrl + 'setSuccess', method:'post', data:{id}})
}

// 拒绝上架
export const setRefused = (id, content)=>{
    return request({url:BaseUrl + 'setRefused', method:'POST', data:{id, content}})
}

// 查看所有用户
export const queryusers = (pageIndex, pageSize)=>{
    return request({url:BaseUrl + 'queryusers', method:'POST', data:{pageIndex, pageSize}})
}

// 添加拍卖会
export const addAuction = (starttime, endtime)=>{
    return request({url:BaseUrl + 'addAuction', method:'POST', data:{starttime, endtime}})
}

// 获取所有拍卖会
export const queryAuction = (pageIndex, pageSize)=>{
    return request({url:BaseUrl + 'queryAuction', method:'POST', data:{pageIndex, pageSize}})
}

// 删除拍卖会
export const delAuction = (ids)=>{
    return request({url:BaseUrl + 'delAuction', method:"POST", data:{ids}})
}

// 修改拍卖会
// 参数 starttime endtime id status
export const updateAuction =  (data)=>{
    return request({url:BaseUrl + 'updateAuction', method:"post", data})
}

// 解密token
export const decodeUsername = (authorization)=>{
    return request({url:BaseUrl+'decodeUsername', method:'post', headers:{authorization}})
}


// 检查是否有相同作品
export const checkSamework = (id)=>{
    return request({url:BaseUrl + 'checkSamework', method:'post', data:{id}})
}


// async function test(){
//     // let result = await decodeUsername("Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDg5MDk1MzYsImRhdGEiOiJhZG1pbiIsImlhdCI6MTY0ODkwODkzNn0.EvgabpOz4C1VcjBzQzd4BPyiHLt6IeAlE-7ghA4_mfg")
//     // let result = await adLogin('admin', 'admin')
//     let result = await checkSamework(23)
//     console.log(result)
// }
// test()