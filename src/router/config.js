import TabsView from '@/layouts/tabs/TabsView'

// 路由配置
const options = {
  routes: [{
      path: '/login',
      name: '登录页',
      component: () => import('@/pages/login')
    },
    {
      path: '*',
      name: '404',
      component: () => import('@/pages/exception/404'),
    },
    {
      path: '/403',
      name: '403',
      component: () => import('@/pages/exception/403'),
    },
    {
      path: '/',
      name: '首页',
      component: TabsView,
      redirect: '/checkartworks',
      children: [

        {
          name: '审核商品',
          path: 'checkartworks',
          meta: {
            icon: 'check-circle-o',
          },
          component: () => import("@/pages/CheckArtworks")
        },
        {
          name: '审核详情',
          path: 'checkdetail/:id',
          meta: {
            invisible: true
          },
          component: () => import("@/pages/CheckDetail")
        },
        {
          name: '驳回成功',
          path: 'success',
          meta: {
            invisible: true
          },
          component: () => import('@/pages/CheckDetail/Success')
        },
        {
          name: "用户管理",
          path: 'admin/user',
          meta: {
            icon: "user"
          },
          component: () => import('@/pages/UserAdmin')
        },
        {
          name: "拍卖时间管理",
          path: 'admin/auctiontime',
          meta: {
            icon: "clock-circle"
          },
          component: () => import('@/pages/AuctionTimeAdmin')
        }
      ]
    },
  ]
}

export default options